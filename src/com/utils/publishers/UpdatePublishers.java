package com.utils.publishers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class UpdatePublishers {

	private static final String FILE = "/Users/christian_velazquez/Desktop/final/inserts.cql";
	private static final String NEW_TABLES_FILE = "/Users/christian_velazquez/Desktop/new_tables.txt";
	private static final String NEW_FILE = "/Users/christian_velazquez/Desktop/final/new_inserts.cql";
	private static final String SQL_FILE = "/Users/christian_velazquez/Desktop/final/new_sql.txt";
	@SuppressWarnings("serial")
	private static List<String> SKIP_TABLES = new ArrayList<String>() {
		{
			add("dbo.properties");
			add("dbo.units");
			add("dbo.amenities");
		}
	};
	
	public static void main(String[] args) throws IOException{
		final List<String> insertStatements = getInsertStatements(FILE);
		final List<String> updatedStatements = new ArrayList<>(insertStatements.size());
		final Map<String, NewTable> newTables = getNewTables(NEW_TABLES_FILE);
		int skippedErrorCount = 0;
		for(String currentStatement : insertStatements) {
			String changeTablename = getChangeTableName(currentStatement);
			if(SKIP_TABLES.contains(changeTablename)) {
				System.out.println("Table in skip list "+changeTablename);
				skippedErrorCount++;
			}else if (changeTablename.equalsIgnoreCase("hct.tracer_token")) {
				System.out.println("Skipping tracer token table "+changeTablename);
				skippedErrorCount++;
			}else {
				try{
					NewTable nt = newTables.get(getApiTableName(changeTablename));
					String newTableAlias = getTableAlias(nt.name);
					currentStatement = addNewJoin(currentStatement, nt);
					currentStatement = replaceUUIDField(currentStatement, getTableAlias(changeTablename), newTableAlias, nt.uuidField);
					currentStatement = replaceUpdateDate(currentStatement, newTableAlias, nt.updateDate);
					//Special cases where additiona API table joins are required
					if(processRefValues(currentStatement)) {
						currentStatement = replaceRefValuesApi(currentStatement);
					}
					if(processRooms(currentStatement)) {
						currentStatement = replaceRoomsApi(currentStatement);
					}
					//
					updatedStatements.add(currentStatement);
				}catch(Exception e) {
					e.printStackTrace();
					System.err.println("Skipping table "+changeTablename+" due to an error");
					skippedErrorCount++;
				}
				generateOutputFile(NEW_FILE, updatedStatements);
				generateOutputFileForSQL(SQL_FILE, updatedStatements);
			}
		}
		System.out.println(String.format("%d statements updated, %d skipped", insertStatements.size()-skippedErrorCount, skippedErrorCount));    
	}
	
	private static void generateOutputFileForSQL(String outputFile, List<String> newStatements) throws IOException {
		Path path = Paths.get(outputFile);
		if(Files.exists(path))
			Files.delete(path);
		try (BufferedWriter writer = Files.newBufferedWriter(path)) {
		    for(String stmt : newStatements) {
		    		System.out.println(stmt);
		    		String subStmt = stmt.substring(stmt.toUpperCase().indexOf("SELECT "));
		    		stmt = subStmt.substring(0, subStmt.indexOf("\""));
		    		stmt = stmt.replaceAll("''", "'").replaceAll("@s", "0");
		    		writer.write(stmt+"\n");
		    }
		} catch (IOException e) {
		    throw e;
		}
	}
	
	private static void generateOutputFile(String outputFile, List<String> newStatements) throws IOException {
		Path path = Paths.get(outputFile);
		if(Files.exists(path))
			Files.delete(path);
		try (BufferedWriter writer = Files.newBufferedWriter(path)) {
		    for(String stmt : newStatements)
			writer.write(stmt+"\n");
		} catch (IOException e) {
		    throw e;
		}
	}
	
	private static List<String> getInsertStatements(String publishersFile) throws IOException{
		final List<String> insertStatements = new ArrayList<>();
		try (BufferedReader reader = Files.newBufferedReader(Paths.get(publishersFile))) {
		    String line = null;
		    StringBuilder currentStatement = new StringBuilder();
		    while ((line = reader.readLine()) != null) {
		    		currentStatement.append(" ").append(line);
		    		if(line.endsWith(");")) {
		    			insertStatements.add(currentStatement.toString());
		    			currentStatement = new StringBuilder();
		    		}	
		    }
		} catch (IOException e) {
		    e.printStackTrace();
		    throw e;
		}
		return insertStatements;
	}
	
	private static Map<String, NewTable> getNewTables(String newTablesFile) throws IOException{
		final Map<String, NewTable> newTables = new HashMap<>();
		try (BufferedReader reader = Files.newBufferedReader(Paths.get(newTablesFile))) {
		    String line = null;
		    while ((line = reader.readLine()) != null) {
		    		String[] tmp = line.split("\\|");
		    		NewTable nt = new NewTable();
		    		nt.name = tmp[0];
		    		nt.keys = tmp[1].split(",");
		    		nt.updateDate = tmp[2];
		    		nt.uuidField = tmp[3];
		    		newTables.put(nt.name, nt);
		    }
		} catch (IOException e) {
		    e.printStackTrace();
		    throw e;
		}
		return newTables;
	}
	
	private static String getChangeTableName(String statement) {
		final String changePart = "CHANGES ";
		final String regexp = changePart + "[\\.a-zA-Z_]*";
		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(statement);
		if (matcher.find())
		{
		    return matcher.group(0).replace(changePart, "");
		}else {
			throw new RuntimeException("No change table name found in statement");
		}
	}
	
	private static String addNewJoin(String statement, NewTable newTableInfo) {
		final String joinString = "INNER JOIN";
		int firstJoinIdx = statement.toUpperCase().indexOf(joinString);
		int secondJoinIdx = statement.toUpperCase().indexOf(joinString, firstJoinIdx + 1);
		String tableAlias = statement.substring(firstJoinIdx, secondJoinIdx).split(" ")[3];
		return statement.substring(0, secondJoinIdx)+getJoinSentence(tableAlias, newTableInfo)+statement.substring(secondJoinIdx);
	}
	
	private static String getJoinSentence(String tableAlias, NewTable newTable) {
		String newAlias = getTableAlias(newTable.name);
		String joinConditions = getJoinConditions(tableAlias, newAlias, newTable);
		return String.format("INNER JOIN %s %s ON %s ", newTable.name, newAlias, joinConditions);
	}
	
	private static String getTableAlias(String tableName) {
		String[] parts = tableName.split("\\.")[1].split("_");
		String alias = "";
		for(String p : parts)
			alias += p.charAt(0);
		return alias;
	}
	
	private static String getJoinConditions(String tableAlias, String newTableAlias, NewTable newTable) {
		String conditions = "";
		for(String k : newTable.keys)
			conditions+= newTableAlias+"."+k+" = "+tableAlias+"."+k+" AND ";
		if(conditions.endsWith("AND "))
			conditions = conditions.substring(0,  conditions.lastIndexOf("AND "));
		return conditions;
	}
	
	private static String getApiTableName(String tableName) {
		return tableName+"_api";
	}
	
	private static String replaceUUIDField(String statement, String alias, String newAlias, String fieldName) {
		String oldField = alias+"."+fieldName;
		String newField = newAlias+"."+fieldName;
		return statement.replace(oldField, newField);
	}
	
	private static String replaceUpdateDate(String statement, String alias, String updateDateField) {
		String dateString = "CONVERT(DATETIME";
		int firstIndex = statement.toUpperCase().indexOf(dateString);
		int secondIndex = statement.toUpperCase().substring(firstIndex).indexOf(")");
		String firstpart = statement.substring(0, firstIndex);
		String secondPart = statement.substring(firstIndex).substring(secondIndex+1);
		return firstpart + alias + "." + updateDateField + secondPart;
	}
	
	private static boolean processRefValues(String statement) {
		return statement.toUpperCase().contains("RV.RVA_UUID");
	}
	
	private static String replaceRefValuesApi(String statement) {
		statement = StringUtils.replace(statement, "rv.rva_uuid", "rva.rva_uuid");
		return addRefValuesJoin(statement);
	}
	
	private static String addRefValuesJoin(String statement) {
		final String joinString = "INNER JOIN";
		int lastJoin = statement.toUpperCase().lastIndexOf(joinString);
		String joinSentence = " INNER JOIN ref_values_api rva ON rva.rva_id = rv.rva_id ";
		return statement.substring(0, lastJoin)+joinSentence+statement.substring(lastJoin);
	}
	
	private static boolean processRooms(String statement) {
		return statement.toUpperCase().contains("R.ROO_UUID");
	}
	
	private static String replaceRoomsApi(String statement) {
		statement = StringUtils.replace(statement, "r.roo_uuid", "ra.roo_uuid");
		return addRoomsJoin(statement);
	}
	
	private static String addRoomsJoin(String statement) {
		final String joinString = "INNER JOIN";
		int lastJoin = statement.toUpperCase().lastIndexOf(joinString);
		String joinSentence = " INNER JOIN dbo.rooms_api ra ON ra.roo_id = r.roo_id  ";
		return statement.substring(0, lastJoin)+joinSentence+statement.substring(lastJoin);
	}
	
}
