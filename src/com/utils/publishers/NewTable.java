package com.utils.publishers;

import java.util.Arrays;

public class NewTable{
	public String name;
	public String[] keys;
	public String updateDate;
	public String uuidField;
	
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof NewTable))
			return false;
		else
			return name.equals((String)o);
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	@Override
	public String toString() {
		return name + ", " + Arrays.toString(keys) + ", " + updateDate;
	}
	
}